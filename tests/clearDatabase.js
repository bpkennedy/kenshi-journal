const admin = require('firebase-admin')
let serviceAccount
const testAccount = {
  created_at: admin.firestore.FieldValue.serverTimestamp(),
  created_by: 'G2v6YCEHn1a1OPsM3ZEnzvy2Cvo2',
  displayName: 'JimmehBoi',
  id: 'G2v6YCEHn1a1OPsM3ZEnzvy2Cvo2',
  updated_at: admin.firestore.FieldValue.serverTimestamp(),
  updated_by: 'G2v6YCEHn1a1OPsM3ZEnzvy2Cvo2',
  theme: null
}

if (!process.env.GITLAB_CI) {
  serviceAccount = require('./rolefiction-uat-admin-sdk.json')
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://rolefiction-uat.firebaseio.com'
  })
} else {
  admin.initializeApp({
    credential: admin.credential.cert({
      'projectId': process.env.VUE_APP_FIREBASE_PROJECT_ID_UAT,
      'clientEmail': process.env.VUE_APP_FIREBASE_CLIENT_EMAIL_UAT,
      'privateKey': process.env.VUE_APP_FIREBASE_PRIVATE_KEY_UAT
    }),
    databaseURL: 'https://rolefiction-uat.firebaseio.com'
  })
}

const db = admin.firestore()

function deleteCollection (db, collectionPath, batchSize) {
  var collectionRef = db.collection(collectionPath)
  var query = collectionRef.orderBy('__name__').limit(batchSize)

  return new Promise((resolve, reject) => {
    deleteQueryBatch(db, query, batchSize, resolve, reject)
  })
}

function deleteQueryBatch (db, query, batchSize, resolve, reject) {
  query.get()
    .then((snapshot) => {
      // When there are no documents left, we are done
      if (snapshot.size === 0) {
        return 0
      }

      // Delete documents in a batch
      var batch = db.batch()
      snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref)
      })

      return batch.commit().then(() => {
        return snapshot.size
      })
    }).then((numDeleted) => {
      if (numDeleted === 0) {
        resolve()
        return
      }

      // Recurse on the next process tick, to avoid
      // exploding the stack.
      process.nextTick(() => {
        deleteQueryBatch(db, query, batchSize, resolve, reject)
      })
    })
    .catch(reject)
}

async function deleteExtraneousUsers () {
  const allUsers = await admin.auth().listUsers(20)
  allUsers.users.forEach(function (userRecord) {
    const user = userRecord.toJSON()
    if (user.uid !== testAccount.id) {
      admin.auth().deleteUser(user.uid)
    }
  })
}

async function resetAdminUser () {
  await db.collection('users').doc(testAccount.id).set(testAccount)
  await db.collection('users').doc(testAccount.id).update({
    entries: [],
    stories: []
  })
}

module.exports = async () => {
  try {
    await deleteCollection(db, 'entries', 500)
    await deleteCollection(db, 'stories', 500)
    await deleteCollection(db, 'users', 500)
    await deleteExtraneousUsers()
    await resetAdminUser()
    return
  } catch (error) {
    console.log('there was an error trying to reset the db')
    console.log(error)
  }
}
