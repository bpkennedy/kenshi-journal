import { t } from 'testcafe'
const resetDatabase = require('./clearDatabase.js')

export async function setup () {
  await teardown()
  await t.eval(() => indexedDB.deleteDatabase('firebaseLocalStorageDb'))
  await t.eval(() => localStorage.clear())
  await t.eval(() => sessionStorage.clear())
  await t.maximizeWindow()
}

export async function teardown () {
  await resetDatabase()
}
