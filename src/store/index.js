import Vue from 'vue'
import Vuex from 'vuex'
import vuexI18n from 'vuex-i18n'
import {
  translationsEn,
  translationsDe,
  translationsFr,
  translationsCs,
  translationsSp,
  translationsJa
} from './translations'
import vuexFirestoreModules from './vuexFirestoreModules'
import config from './modules/config'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    config
  },
  plugins: [
    vuexFirestoreModules
  ]
})

Vue.use(vuexI18n.plugin, store)

Vue.i18n.add('en', translationsEn)
Vue.i18n.add('de', translationsDe)
Vue.i18n.add('fr', translationsFr)
Vue.i18n.add('cs', translationsCs)
Vue.i18n.add('sp', translationsSp)
Vue.i18n.add('ja', translationsJa)
Vue.i18n.set('en')

export default store
