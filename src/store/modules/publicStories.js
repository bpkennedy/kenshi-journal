import { arrayUnion, arrayRemove } from 'vuex-easy-firestore'

const getDefaultState = () => {
  return {
    publicStories: {}
  }
}

const state = getDefaultState()

const getters = {
  publicStories: ({ publicStories }) => Object.values(publicStories).sort((first, second) => second.created_at - first.created_at).filter(story => story.entries && story.entries.length > 0),
  storyById: ({ publicStories }) => (id) => Object.values(publicStories).find(story => story.id === id)
}

const actions = {
  async getStoryById ({ getters }, storyId) {
    return getters.storyById(storyId)
  },
  async likeStory ({ dispatch }, { storyId, like }) { // eslint-disable-line camelcase
    await dispatch('patch', {
      id: storyId,
      likes: arrayUnion(like)
    })
  },
  async unLikeStory ({ dispatch }, { storyId, like }) { // eslint-disable-line camelcase
    await dispatch('patch', {
      id: storyId,
      likes: arrayRemove(like)
    })
  },
  async resetPublicStoriesState ({ commit, dispatch }) {
    await dispatch('closeDBChannel', { clearModule: true })
    commit('resetState', null)
  }
}

const mutations = {
  resetState (state) {
    const defaultState = getDefaultState()
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export default {
  firestorePath: 'stories',
  firestoreRefType: 'collection',
  moduleName: 'publicStoriesData',
  statePropName: 'publicStories',
  namespaced: true,
  serverChange: {
    convertTimestamps: {
      created_at: '%convertTimestamp%',
      updated_at: '%convertTimestamp%'
    }
  },
  sync: {
    where: [['private', '==', false]],
    orderBy: ['created_at'],
    debounceTimerMs: 0
  },
  state,
  getters,
  actions,
  mutations
}
