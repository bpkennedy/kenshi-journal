import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { firebase } from './firebase'
import { initializeFirestoreModules, closeFirestoreModules } from './store/vuexFirestoreModules'
import './registerServiceWorker'
import './plugins/element.js'
import { initializeAnalytics } from './plugins/analytics.js'

Vue.config.productionTip = false

let app = ''

firebase.auth().onAuthStateChanged(async (user) => {
  if (user) {
    try {
      await initializeFirestoreModules(store)
      await store.commit(`userData/login`, null)
      initializeAnalytics(user.uid, router)
    } catch (error) {
      console.log(error)
    }
  } else {
    await closeFirestoreModules(store)
    await store.commit(`userData/logout`)
  }

  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
